Create TABLE pessoa(
codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
nome VARCHAR(50) NOT NULL,
logradouro VARCHAR(100),
numero VARCHAR(10),
complemento VARCHAR(50),
bairro VARCHAR(50),
cep VARCHAR(9),
cidade VARCHAR(50),
Estado VARCHAR(20),
ativo boolean NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo) value ('Luis Renato Coelho', 'Rua João Marques', '286', 'casa', 'Parque Santo Antonio', '61880-000', 'Itaitinga', 'Ceará', true);
INSERT INTO pessoa (nome, logradouro, numero, complemento, bairro, cep, cidade, estado, ativo) value ('Maria Eulenilde Pereira Coelho', 'Rua João Marques', '286', 'casa', 'Parque Santo Antonio', '61880-000', 'Itaitinga', 'Ceará', true);