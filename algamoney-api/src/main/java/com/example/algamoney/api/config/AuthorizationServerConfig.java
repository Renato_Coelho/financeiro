package com.example.algamoney.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;


@Profile("oauth-security")
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	/**
	 * Configurações das credenciais
	 * do cliente 
	 */
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
			.withClient("angular")
			.secret("@ngul@r0")
			//necessário definir senão gera erro
			.scopes("read" , "write") //É possível definir varios escopos
			.authorizedGrantTypes("password","refresh_token")
			//Tempo de acesso do token em segundos
			.accessTokenValiditySeconds(1800)
			.refreshTokenValiditySeconds(3600 *24) // Tempo de vida do refresh token 1 dia.
		.and()
			.withClient("mobile")
			.secret("m0b1l30")
			.scopes("read") //este cliente só tem acesso leitura
			.authorizedGrantTypes("password","refresh_token")
			.accessTokenValiditySeconds(1800)
			.refreshTokenValiditySeconds(3600 *24);
	}
	

}
